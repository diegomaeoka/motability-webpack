// import { document, console } from "global";
import { storiesOf } from "@storybook/html";
// import { action } from "@storybook/addon-actions";
import { checkA11y } from '@storybook/addon-a11y';

import $ from 'jquery';
import 'bootstrap';
window.jQuery = $;
window.$ = $;

import "../main_story.scss";
import advancedSearch from './advanced-search.html';

storiesOf("Advanced Search", module)
  .addDecorator(checkA11y)
  .add("Default", () => advancedSearch)
  .add("Colcon", () => {
    const div = document.createElement('div');
    div.innerHTML = `<div class="colcon">${advancedSearch}</div>`;
    return div;
  })
  .add("Dark", () => {
    const div = document.createElement('div');
    div.innerHTML = `<div class="dark">${advancedSearch}</div>`;
    return div;
  })
