// import { document, console } from "global";
import { storiesOf } from "@storybook/html";
// import { action } from "@storybook/addon-actions";
import { checkA11y } from '@storybook/addon-a11y';

import $ from 'jquery';
import 'bootstrap';
window.jQuery = $;
window.$ = $;

import "../main_story.scss";
import alerts from './alerts.html';

storiesOf("Alerts", module)
  .addDecorator(checkA11y)
  .add("Default", () => alerts)
  .add("Colcon", () => {
    const div = document.createElement('div');
    div.innerHTML = `<div class="colcon">${alerts}</div>`;
    return div;
  })
  .add("Dark", () => {
    const div = document.createElement('div');
    div.innerHTML = `<div class="dark">${alerts}</div>`;
    return div;
  })
