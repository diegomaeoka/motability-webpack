// import { document, console } from "global";
import { storiesOf } from "@storybook/html";
// import { action } from "@storybook/addon-actions";
import { withLinks } from "@storybook/addon-links";
// import { checkA11y } from '@storybook/addon-a11y';

import $ from 'jquery';
import 'bootstrap';
window.jQuery = $;
window.$ = $;

import "./main_story.scss";
import welcome from "./welcome.html";
// import alerts from "./alerts.html";
// import buttons from "./buttons.html";
// import advancedSearch from './advanced-search.html';

storiesOf("Getting Started", module)
  .addDecorator(withLinks)
  .add("Welcome", () => welcome);

// storiesOf("Components", module)
//   .addDecorator(checkA11y)
//   .add("Alerts", () => alerts)
//   .add("Buttons", () => buttons)
//   .add("Advanced Search", () => advancedSearch);
