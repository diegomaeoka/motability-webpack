const path = require('path');
const {
    argv,
} = require('yargs');
const merge = require('webpack-merge');
const isProduction = !!((argv.env && argv.env.production) || argv.p);
const rootPath = process.cwd();

const config = {
    open: false,
    copy: [
        'images/**/*',
        'fonts/**/*',
    ],
    proxyUrl: 'http://localhost:9000',
    cacheBusting: '[name]_[hash]',
    paths: {
        root: rootPath,
        assets: path.join(rootPath, 'assets'),
        dist: path.join(rootPath, 'dist'),
    },
    enabled: {
        sourceMaps: !isProduction,
        optimize: isProduction,
        cacheBusting: isProduction,
        watcher: !!argv.watch,
    },
    fileList: [
      "index.html",
      "about.html",
    ],
    watch: [
        "scripts/**/*.js",
        "styles/**/*.scss",
    ],
};

module.exports = merge(config, {
    env: Object.assign({
        production: isProduction,
        development: !isProduction,
    }, argv.env),
    publicPath: '',
    manifest: {},
});

if (process.env.NODE_ENV === undefined) {
    process.env.NODE_ENV = isProduction ? 'production' : 'development';
}

if (process.env.DIST_PATH) {
    module.exports.publicPath = process.env.DIST_PATH;
}

// Object.keys(module.exports.entry).forEach(id =>
//   module.exports.entry[id].unshift(path.join(__dirname, 'helpers/public-path.js')));
