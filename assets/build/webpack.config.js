/* eslint-disable global-require */
'use strict';

const webpack = require('webpack');
const merge = require('webpack-merge');
const CleanPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const CopyGlobsPlugin = require('copy-globs-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const desire = require('./util/desire');
const config = require('./config');
const {version} = require('../../version');
const assetsFilenames = '[name]';

let webpackConfig = {
    context: config.paths.assets,
    entry: {
        vendor: [
            "bootstrap",
            "popper.js/dist/umd/popper.js",
            "jquery",
        ],
        main: [
            "./scripts/main.js",
            "./styles/main.scss",
        ],
    },
    devtool: (config.enabled.sourceMaps ? '#source-map' : undefined),
    output: {
        path: config.paths.dist,
        publicPath: "/",
        filename: `scripts/${assetsFilenames}_${version}.js`,
    },
    stats: {
        hash: false,
        version: false,
        timings: false,
        children: false,
        errors: false,
        errorDetails: false,
        warnings: false,
        chunks: false,
        modules: false,
        reasons: false,
        source: false,
        publicPath: false,
    },
    module: {
        rules: [{
                enforce: 'pre',
                test: /\.js$/,
                include: config.paths.assets,
                use: 'eslint',
            },
            {
                enforce: 'pre',
                test: /\.(js|s?[ca]ss)$/,
                include: config.paths.assets,
                loader: 'import-glob',
            },
            {
                test: /\.js$/,
                exclude: [/node_modules(?![/|\\](bootstrap|foundation-sites))/],
                use: [{
                        loader: 'cache',
                    },
                    {
                        loader: 'buble',
                        options: {
                            objectAssign: 'Object.assign',
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                include: config.paths.assets,
                use: ExtractTextPlugin.extract({
                    fallback: 'style',
                    use: [{
                            loader: 'cache',
                        },
                        {
                            loader: 'css',
                            options: {
                                sourceMap: config.enabled.sourceMaps,
                            },
                        },
                        {
                            loader: 'postcss',
                            options: {
                                config: {
                                    path: __dirname,
                                    ctx: config,
                                },
                                sourceMap: config.enabled.sourceMaps,
                            },
                        },
                    ],
                }),
            },
            {
                test: /\.scss$/,
                include: config.paths.assets,
                use: ExtractTextPlugin.extract({
                    fallback: 'style',
                    use: [{
                            loader: 'cache',
                        },
                        {
                            loader: 'css',
                            options: {
                                sourceMap: config.enabled.sourceMaps,
                            },
                        },
                        {
                            loader: 'postcss',
                            options: {
                                config: {
                                    path: __dirname,
                                    ctx: config,
                                },
                                sourceMap: config.enabled.sourceMaps,
                            },
                        },
                        {
                            loader: 'resolve-url',
                            options: {
                                sourceMap: config.enabled.sourceMaps,
                            },
                        },
                        {
                            loader: 'sass',
                            options: {
                                sourceMap: config.enabled.sourceMaps,
                                sourceComments: true,
                            },
                        },
                    ],
                }),
            },
            {
                test: /\.(ttf|otf|eot|woff2?|png|jpe?g|gif|svg|ico)$/,
                include: config.paths.assets,
                loader: 'url',
                options: {
                    limit: 4096,
                    name: `[path]${assetsFilenames}.[ext]`,
                },
            },
            {
                test: /\.(ttf|otf|eot|woff2?|png|jpe?g|gif|svg|ico)$/,
                include: /node_modules/,
                loader: 'url',
                options: {
                    limit: 4096,
                    outputPath: 'vendor/',
                    name: `${config.cacheBusting}.[ext]`,
                },
            },
        ],
    },
    resolve: {
        modules: [
            config.paths.assets,
            'node_modules',
        ],
        enforceExtension: false,
    },
    resolveLoader: {
        moduleExtensions: ['-loader'],
    },
    plugins: [
        new CleanPlugin([config.paths.dist], {
            root: config.paths.root,
            verbose: false,
        }),
        new ExtractTextPlugin({
            filename: `styles/${assetsFilenames}_${version}.css`,
            allChunks: true,
            disable: (config.enabled.watcher),
        }),
        new webpack.ProvidePlugin({
            '$':'jquery/dist/jquery.js',
            'jQuery':'jquery/dist/jquery.js',
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: `scripts/vendor.bundle.js`,
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: config.enabled.optimize,
            debug: config.enabled.watcher,
            stats: {
                colors: true,
            },
        }),
        new webpack.LoaderOptionsPlugin({
            test: /\.s?css$/,
            options: {
                output: {
                    path: config.paths.dist,
                },
                context: config.paths.assets,
            },
        }),
        new webpack.LoaderOptionsPlugin({
            test: /\.js$/,
            options: {
                eslint: {
                    failOnWarning: false,
                    failOnError: true,
                },
            },
        }),
        new StyleLintPlugin({
            failOnError: !config.enabled.watcher,
            syntax: 'scss',
        }),
        new FriendlyErrorsWebpackPlugin(),
    ],
};

if (config.enabled.optimize) {
    webpackConfig = merge(webpackConfig, require('./webpack.config.optimize'));
}

if (config.env.production) {
    webpackConfig.plugins.push(new webpack.NoEmitOnErrorsPlugin());
}

if (config.enabled.cacheBusting) {
    const WebpackAssetsManifest = require('webpack-assets-manifest');
    webpackConfig.plugins.push(
        new WebpackAssetsManifest({
            output: 'assets.json',
            space: 2,
            writeToDisk: false,
            assets: config.manifest,
            replacer: require('./util/assetManifestsFormatter'),
        })
    );
}

if (config.copy.length > 0) {
  config.copy.forEach(function(element) {
      webpackConfig.plugins.push(
        new CopyGlobsPlugin({
          pattern: element,
          output: `[path]${assetsFilenames}.[ext]`,
          manifest: config.manifest,
        })
      )
  });
}

if (config.fileList.length > 0) {
    config.fileList.forEach(function(element) {
        webpackConfig.plugins.push(
            new HtmlWebpackPlugin({
                hash: true,
                minify:{
                    html5: true,
                    collapseWhitespace: true,
                    removeComments: true,
                },
                filename: element,
                template: config.paths.assets + `/html/${element}`,
            })
        )
    });
}

if (config.enabled.watcher) {
    webpackConfig.entry = require('./util/addHotMiddleware')(webpackConfig.entry);
    webpackConfig = merge(webpackConfig, require('./webpack.config.watch'));
}

module.exports = merge(webpackConfig, desire(`${__dirname}/webpack.config.preset`));
