/* eslint-env browser */
/* globals DIST_PATH */

/** Dynamically set absolute public path from current protocol and host */
if (DIST_PATH) {
  __webpack_public_path__ = DIST_PATH; // eslint-disable-line no-undef, camelcase
}
