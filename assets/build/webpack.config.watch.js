const webpack = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const config = require('./config');

let plugins = [];
plugins.push(new webpack.optimize.OccurrenceOrderPlugin());
plugins.push(new webpack.HotModuleReplacementPlugin());
plugins.push(new webpack.NoEmitOnErrorsPlugin());
plugins.push(new BrowserSyncPlugin({
  files: 'dist/**/*.*',
  hostname: 'localhost',
  port: 9000,
  server: { baseDir: ['dist'] },
  reloadDelay: 50,
  injectChanges: false,
  reloadDebounce: 500,
  reloadOnRestart: true,
}));

module.exports = {
  output: {
    pathinfo: true,
    // publicPath: config.proxyUrl + config.publicPath,
  },
  devtool: '#cheap-module-source-map',
  devServer: {
    port: 9000,
    contentBase: config.paths.dist,
  },
  stats: false,
  plugins,
};
