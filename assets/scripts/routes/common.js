export default {
  init() {
    megaMenu();
  },
  finalize() {
    $(window).resize();
  },
};

const megaMenu = () => {
  let $width = $(window).width();
  const $menu = $('.mega-menu');
  $(window).resize(() => {
    if ($width >= 980) {
      $menu.on('hover','.dropdown-toggle', () => {
        $(this).parent().toggleClass("show");
        $(this).parent().find(".dropdown-menu").toggleClass("show");
      });
    }
    $menu.on('mouseleave','.dropdown-menu', () => {
      $(this).removeClass("show");
    });
  });
}
